import React, { Component } from 'react';
import { render } from 'react-dom';
import Hello from './Hello';
import './style.css';

class App extends Component {
  constructor() {
    super();
    this.state = {
      name: 'Users',
      isloaded: false,
      data:[],
    };
  }

  componentWillMount()
  {
    console.log("Fun mounted");
    fetch("https://jsonplaceholder.typicode.com/users")
    .then(res => res.json())
    .then((result) => {
      this.setState({data: result});
      console.log(result.items)
    },
    (error) => {
      console.log(error)
    })
  }

  renderData = () => {
    const dataArray = this.state.data;
    return(
      dataArray.map( (elem, index) => 
       (<li key={index}>Name: {elem.name}, &nbsp;&nbsp;
          Username: {elem.username}, &nbsp;&nbsp;
          Street: {elem.address.street}, &nbsp;&nbsp;
          Suite: {elem.address.suite}, &nbsp;&nbsp;
          City: {elem.address.city}, &nbsp;&nbsp;
          Zip: {elem.address.zipcode}
          <br/><br/>
       </li>)
    )
    );
  }

  render() {

    return (
      <div>
        <h2>{this.state.name}</h2>
        <ul>
          {this.renderData()}
        </ul>
      </div>
    );
  }
}

render(<App />, document.getElementById('root'));
